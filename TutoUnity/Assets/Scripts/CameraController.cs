﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public GameObject player;

    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        // Compute the distance between the 2 objects
        offset = transform.position - player.transform.position;
    }

    // Same as "Update" method but process after every GameObject's "Update" finished
    void LateUpdate()
    {
        // Keep the camera to the same (constant) distance from the player
        transform.position = player.transform.position + offset;
    }
}

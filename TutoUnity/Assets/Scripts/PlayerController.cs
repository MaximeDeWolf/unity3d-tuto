﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Allows the usage of the UI's objects
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // public variables are editables in the GUI
    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    // Start is called only in the first frame
    void Start()
    {
        // Get the RigidBody of our player
        rb = GetComponent<Rigidbody>();
        count = 0;
        updateCountText();
        winText.text = "";
    }

    // Update is called once per frame, just before rendering the object
    // => apply visual transformation
    void Update()
    {
        
    }

    // Update is called once per frame, just before applying any physics calculations
    // => apply physics code
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f,  moveVertical);
        // Apply the movement to the rigigBody
        rb.AddForce(movement * speed);
    }

    // It is called when the object collide with another one
    private void OnTriggerEnter(Collider other)
    {
        // Tags have to be declared in advance
        if (other.gameObject.CompareTag("Pick Up"))
        {
            // We prefer to desactivate the object rather than destroy it in this example
            //Destroy(other.gameObject);
            other.gameObject.SetActive(false);
            count += 1;
            updateCountText();
        }
    }

    private void updateCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 10)
        {
            winText.text = "You win !";
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        // The vector represents the rotation around each axis
        // We multiply it by "deltatime" to make the rotation smouth
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
}

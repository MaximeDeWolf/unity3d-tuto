# Unity3D-Tuto

This project content all the source code obtained by following this tutorial: https://unity3d.com/fr/learn/tutorials/projects/roll-ball-tutorial/introduction-roll-ball?playlist=17141.
This game has been made thanks to *Unity 2018.3.6f1*.
It is a simple game where the player controlls a ball and have to collect several items to win the level.

## Features
We list here all the *Unity* features we learn during this tutorial:
1.  Simple physics, light and camera
2.  Add controlls to an entity
3.  Basic prefabs
4.  Trigger an event when a colision occurs
5.  Optimisation trick to save some cache
6.  Good repository structure to keep the project clean

If want more informations on these, feel free to read the *CHANGELOGS* and the comments in the code files.